use std::fs::read_to_string;

fn main() {

    let lines = read_lines("../input");
    let mut score: usize = 0;

    for line_index in 0..lines.len() {

        for symbol_index in 0..lines[line_index].len() {

            let symbol = lines[line_index].as_bytes()[symbol_index];

            if (32 < symbol && symbol < 46) || symbol == 47 || symbol == 61 || symbol == 64 {

                if 47 < lines[line_index-1].as_bytes()[symbol_index] && lines[line_index-1].as_bytes()[symbol_index] < 58 {

                    score += search_all(&lines, line_index-1, symbol_index);            

                } else {
                    
                    if 47 < lines[line_index-1].as_bytes()[symbol_index-1] && lines[line_index-1].as_bytes()[symbol_index-1] < 58 {

                        score += search_all(&lines, line_index-1, symbol_index-1);

                    }
                    if 47 < lines[line_index-1].as_bytes()[symbol_index+1] && lines[line_index-1].as_bytes()[symbol_index+1] < 58 {

                        score += search_right(&lines, line_index-1, symbol_index+1);

                    }

                }

                if 47 < lines[line_index].as_bytes()[symbol_index+1] && lines[line_index].as_bytes()[symbol_index+1] < 58 {

                    score += search_right(&lines, line_index, symbol_index+1);

                }

                if 47 < lines[line_index+1].as_bytes()[symbol_index] && lines[line_index+1].as_bytes()[symbol_index] < 58 {

                    score += search_all(&lines, line_index+1, symbol_index);

                } else {
                    
                    if 47 < lines[line_index+1].as_bytes()[symbol_index-1] && lines[line_index+1].as_bytes()[symbol_index-1] < 58 {

                        score += search_all(&lines, line_index+1, symbol_index-1);

                    }
                    if 47 < lines[line_index+1].as_bytes()[symbol_index+1] && lines[line_index+1].as_bytes()[symbol_index+1] < 58 {

                        score += search_right(&lines, line_index+1, symbol_index+1);

                    }

                }


                if 47 < lines[line_index].as_bytes()[symbol_index-1] && lines[line_index].as_bytes()[symbol_index+1] < 58 {

                    score += search_all(&lines, line_index, symbol_index-1);

                }


            }

        }

    }

    println!("{}", score);

}

fn search_all(lines: &Vec<String>, line: usize, current_index: usize) -> usize {

    let mut first_index = current_index;

    loop {
        if !(47 < lines[line].as_bytes()[first_index] && lines[line].as_bytes()[first_index] < 58)  {
            first_index += 1;
            break;
        }
        if first_index == 0 {
            break;
        }

        first_index -= 1;

    }

    search_right(&lines, line, first_index)
    
}

fn search_right(lines: &Vec<String>, line: usize, mut index: usize) -> usize {

    let mut number: usize = 0;
                    
    while 47 < lines[line].as_bytes()[index] && lines[line].as_bytes()[index] < 58 { 

        number *= 10;
        number += (lines[line].as_bytes()[index] - 48) as usize;
        index += 1;
        
        if index == lines.len() {
            break;
        }

    }

    number

}

fn read_lines(filename: &str) -> Vec<String> {

    let mut result = Vec::new();

    for line in read_to_string(filename).unwrap().lines() {
        result.push(line.to_string())
    }

    result  
}
