use std::fs::read_to_string;
use regex::Regex;

fn main() {

    let lines = read_lines("../input");

    
    let mut score:usize = 0;

    for line in lines.iter() {

        score += check_line(&line);

    }

    println!("{}", score);

}

fn check_line(line: &str) -> usize {

    let regex_color_amount: Regex = Regex::new(r"([0-9]+ [a-z]+)").unwrap();
    let regex_red = Regex::new(r"([0-9]+) red").unwrap();
    let regex_green = Regex::new(r"([0-9]+) green").unwrap();
    let regex_blue = Regex::new(r"([0-9]+) blue").unwrap();

    let mut score_red = 0;
    let mut score_green = 0;
    let mut score_blue = 0;

    for (_, [color_amount]) in regex_color_amount.captures_iter(line).map(|c| c.extract()) {

            if let Some(red_amount_string) = regex_red.captures(color_amount) {

                let red_amount = red_amount_string[1].parse::<usize>().unwrap();
                if red_amount > score_red { score_red = red_amount};

            } else if let Some(green_amount_string) = regex_green.captures(color_amount) {

                let green_amount = green_amount_string[1].parse::<usize>().unwrap();
                if green_amount > score_green { score_green = green_amount};

            } else if let Some(blue_amount_string) = regex_blue.captures(color_amount) {

                let blue_amount = blue_amount_string[1].parse::<usize>().unwrap();
                if blue_amount > score_blue { score_blue = blue_amount};

            }

    }

    return score_red * score_green * score_blue;

}

fn read_lines(filename: &str) -> Vec<String> {

    let mut result = Vec::new();

    for line in read_to_string(filename).unwrap().lines() {
        result.push(line.to_string())
    }

    result  
}
