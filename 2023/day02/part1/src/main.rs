use std::fs::read_to_string;
use regex::Regex;

const MAX_RED: usize = 12;
const MAX_GREEN: usize = 13;
const MAX_BLUE: usize = 14;

fn main() {

    let lines = read_lines("../input");

    

    let mut score:usize = 0;
    let mut counter: usize = 1;

    for line in lines.iter() {

        check_line(&line, &mut counter, &mut score);

    }

    println!("{}", score);

}

fn check_line(line: &str, counter: &mut usize, score: &mut usize) {

    let regex_color_amount: Regex = Regex::new(r"([0-9]+ [a-z]+)").unwrap();
    let regex_red = Regex::new(r"([0-9]+) red").unwrap();
    let regex_green = Regex::new(r"([0-9]+) green").unwrap();
    let regex_blue = Regex::new(r"([0-9]+) blue").unwrap();

    for (_, [color_amount]) in regex_color_amount.captures_iter(line).map(|c| c.extract()) {

            if let Some(red_amount) = regex_red.captures(color_amount) {
                if red_amount[1].parse::<usize>().unwrap() > MAX_RED {
                    *counter += 1;
                    return;
                }
            } else if let Some(green_amount) = regex_green.captures(color_amount) {
                if green_amount[1].parse::<usize>().unwrap() > MAX_GREEN {
                    *counter += 1;
                    return;
                }
            } else if let Some(blue_amount) = regex_blue.captures(color_amount) {
                if blue_amount[1].parse::<usize>().unwrap() > MAX_BLUE {
                    *counter += 1;
                    return;
                }
            }

        }

        *score += *counter;
        *counter += 1;

}

fn read_lines(filename: &str) -> Vec<String> {

    let mut result = Vec::new();

    for line in read_to_string(filename).unwrap().lines() {
        result.push(line.to_string())
    }

    result  
}
