use std::fs::read_to_string;

fn main() {

    let lines = read_lines("../input");

    let mut res: u16 = 0;

    for line in lines.iter() {

        let mut first = 0;
        let mut last = 0;

        for i in 0..line.len() {

            if first == 0{
                let left = line.as_bytes()[i];
                if left > 47 && left < 58 {
                    first = left - 48;
                }
            }
            if last == 0 {
                let right = line.as_bytes()[line.len()-1 - i];
                if right > 47 && right < 58 {
                    last = right - 48;
                }
            }

        }

        res += (first * 10 + last) as u16;

    }

    println!("{}", res);

}

fn read_lines(filename: &str) -> Vec<String> {

    let mut result = Vec::new();

    for line in read_to_string(filename).unwrap().lines() {
        result.push(line.to_string())
    }

    result  
}
