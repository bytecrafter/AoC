use std::fs::read_to_string;

fn main() {

    let lines = read_lines("../input");

    let mut res: u16 = 0;

    for line in lines.iter() {

        let mut first = 0; // kinda risky if there are 0's... luckily there are none x3
        let mut last = 0;

        for i in 0..line.len() {

            if first == 0 {
                let left = line.as_bytes()[i];
                if left > 47 && left < 58 {
                    first = left - 48;
                } else {
                    first = check_letter_front(&line, i);
                }
            }
            if last == 0 {
                let right = line.as_bytes()[line.len()-1 - i];
                if right > 47 && right < 58 {
                    last = right - 48;
                } else {
                    last = check_letter_back(&line, line.len()-1 - i);
                }
            }

        }

        res += (first * 10 + last) as u16;

    }

    println!("{}", res);

}


fn check_letter_front(string: &String, index: usize) -> u8 {


    if string.len() - index > 2 {
        match &string[index..index+1] {

            "o" => if &string[index..index+3] == "one" { return 1 },
            "t" => if &string[index..index+3] == "two" { return 2 },
            "s" => if &string[index..index+3] == "six" { return 6 },
            _ => print!(""),

        }
    } else { return 0 }


    if string.len() - index > 3 {
        match &string[index..index+1] {

            "f" => if &string[index..index+4] == "four" { return 4 }
                   else if &string[index..index+4] == "five" { return 5 },
            "n" => if &string[index..index+4] == "nine" { return 9 },
            _ => print!(""),

        }
    } else { return 0; }

    if string.len() - index > 4 {
        match &string[index..index+1] {

            "t" => if &string[index..index+5] == "three" { return 3 },
            "e" => if &string[index..index+5] == "eight" { return 8 },
            "s" => if &string[index..index+5] == "seven" { return 7 },
            _ => print!(""),

        }
    }
    
    0

}


fn check_letter_back(string: &String, index: usize) -> u8 {


    if index >= 2 {
        match &string[index..index+1] {

            "e" => if &string[index-2..index+1] == "one" { return 1 },
            "o" => if &string[index-2..index+1] == "two" { return 2 },
            "x" => if &string[index-2..index+1] == "six" { return 6 },
            _ => print!(""),

        }
    } else { return 0 }


    if index >= 3 {
        match &string[index..index+1] {

            "e" => if &string[index-3..index+1] == "nine" { return 9 }
                   else if &string[index-3..index+1] == "five" { return 5 },
            "r" => if &string[index-3..index+1] == "four" { return 4 },
            _ => print!(""),

        }
    } else { return 0; }

    if index >= 4 {
        match &string[index..index+1] {

            "e" => if &string[index-4..index+1] == "three" { return 3 },
            "t" => if &string[index-4..index+1] == "eight" { return 8 },
            "n" => if &string[index-4..index+1] == "seven" { return 7 },
            _ => print!(""),

        }
    }
    
    0

}



fn read_lines(filename: &str) -> Vec<String> {

    let mut result = Vec::new();

    for line in read_to_string(filename).unwrap().lines() {
        result.push(line.to_string())
    }

    result  
}
