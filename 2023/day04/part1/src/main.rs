use std::fs::read_to_string;

fn main() {

    let lines = read_lines("../input");
    let mut score = 0;

    for line in lines.iter() {

        let mut target_numbers = Vec::new();
        let mut card_numbers = Vec::new();

        let split_line = line.split(" ");
        let mut is_left_of_seperator: bool = true;

        for word in split_line {

            if word == "Card" || word.contains(":") { continue }
            if word == "|" {
                is_left_of_seperator = false;
                continue
            }
            
            if word == "" { continue; }

            if is_left_of_seperator {
                target_numbers.push(word) 
            } else {
                card_numbers.push(word)
            }


        }

        let mut card_matches = 0;

        for number in card_numbers {

            if target_numbers.contains(&number) {
                card_matches += 1;
                //println!("{}", &number);
            }
            
        }

        if card_matches != 0 { score += (2 as usize).pow(card_matches - 1); }

    }

    println!("{}", score);

}

fn read_lines(filename: &str) -> Vec<String> {

    let mut result = Vec::new();

    for line in read_to_string(filename).unwrap().lines() {
        result.push(line.to_string())
    }

    result  
}
