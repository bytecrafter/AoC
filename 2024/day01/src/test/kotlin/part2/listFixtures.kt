package part2

val leftListFixture = listOf(1, 2, 3, 3, 3, 4)
val rightListFixture = listOf(3, 3, 3, 4, 5, 9)
val similarityScoresFixture = listOf(0, 0, 9, 9, 9, 4)