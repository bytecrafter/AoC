package part2

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

class MainTest {

    @Test
    fun `should properly read input and return it as a list`() {
        val input = getInput(object {}::class.java.getResource("../test_input")?.toURI())

        assertThat( //yes dis sorta bullshit, don't care lol
            input.toString().replace(", ", "\n").replace("[\\[\\]]".toRegex(), "")
        ).isEqualTo(File(object {}::class.java.getResource("../test_input")!!.toURI()).readText())
    }

    @Test
    fun `should properly transform the input into two lists`() {
        val input = getInput(object {}::class.java.getResource("../test_input")?.toURI())
        val twoLists = transformIntoLists(input)
        assertThat(twoLists).isEqualTo(listOf(leftListFixture, rightListFixture))
    }

    @Test
    fun `should correctly calculate the similarity scores`() {
        val similarityScores = calculateSimilarityScores(leftListFixture, rightListFixture)
        assertThat(similarityScores).isEqualTo(similarityScoresFixture)
    }

    @Test
    fun `should calculate the correct resulting score`() {
        val score = calculateScore(similarityScoresFixture)
        assertThat(score).isEqualTo(31)
    }

}
