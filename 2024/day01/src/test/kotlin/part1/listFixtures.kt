package part1

val leftListFixture = listOf(1, 2, 3, 3, 3, 4)
val rightListFixture = listOf(3, 3, 3, 4, 5, 9)
val distancesFixture = listOf(2, 1, 0, 1, 2, 5)