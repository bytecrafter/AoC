package part1

import java.io.File
import java.net.URI
import kotlin.math.absoluteValue


fun main() {
    val input = getInput(object {}::class.java.getResource("../input")?.toURI())
    val leftAndRightList = transformIntoLists(input)
    val distances = calculateDistances(leftAndRightList[0], leftAndRightList[1])
    println(calculateTotalDistance(distances))
}

fun getInput(inputURI: URI?): List<String> {
    requireNotNull(inputURI) { "Input URI cannot be null" }
    return File(inputURI).readLines()
}

fun transformIntoLists(input: List<String>): List<List<Int>> {
    val leftList = mutableListOf<Int>()
    val rightList = mutableListOf<Int>()
    input.forEach {
        val split = it.split("   ")
        leftList.add(split[0].toInt())
        rightList.add(split[1].toInt())
    }
    return listOf(leftList.sorted(), rightList.sorted())
}

fun calculateDistances(left: List<Int>, right: List<Int>): List<Int> = buildList {
    for (i in left.indices) {
        add((left[i] - right[i]).absoluteValue)
    }
}

fun calculateTotalDistance(distances: List<Int>): Int = distances.sum()
