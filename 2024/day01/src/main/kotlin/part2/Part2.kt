package part2

import java.io.File
import java.net.URI


fun main() {
    val input = getInput(object {}::class.java.getResource("../input")?.toURI())
    val leftAndRightList = transformIntoLists(input)
    println(calculateScore(calculateSimilarityScores(leftAndRightList[0], leftAndRightList[1])))

}

fun getInput(inputURI: URI?): List<String> {
    requireNotNull(inputURI) { "Input URI cannot be null" }
    return File(inputURI).readLines()
}

fun transformIntoLists(input: List<String>): List<List<Int>> {
    val leftList = mutableListOf<Int>()
    val rightList = mutableListOf<Int>()
    input.forEach {
        val split = it.split("   ")
        leftList.add(split[0].toInt())
        rightList.add(split[1].toInt())
    }
    return listOf(leftList.sorted(), rightList.sorted())
}

fun calculateSimilarityScores(leftList: List<Int>, rightList: List<Int>): List<Int> = buildList {
    leftList.forEach { leftVal ->
        add(leftVal * rightList.count { it == leftVal })
    }
}

fun calculateScore(similarityScores: List<Int>): Int = similarityScores.sum()
