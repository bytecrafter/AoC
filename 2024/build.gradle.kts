plugins {

    alias(libs.plugins.kotlin.plugin)

}

allprojects {

    repositories {
        mavenLocal()
        mavenCentral()
    }

}

subprojects {

    apply {
        plugin("application")
        plugin(rootProject.project.libs.plugins.kotlin.plugin.get().pluginId)
    }

    dependencies {
        testImplementation(rootProject.project.libs.junit)
        testImplementation(rootProject.project.libs.assertj)
    }

    tasks.test {
        useJUnitPlatform()
    }

}
