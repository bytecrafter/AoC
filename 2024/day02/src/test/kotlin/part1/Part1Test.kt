package part1

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.io.File

class MainTest {

    @Test
    fun `should properly read input and return it as a list`() {
        val input = getInput(object {}::class.java.getResource("../test_input")?.toURI())

        assertThat( //yes dis sorta bullshit, don't care lol
            input.toString().replace(", ", "\n").replace("[\\[\\]]".toRegex(), "")
        ).isEqualTo(File(object {}::class.java.getResource("../test_input")!!.toURI()).readText())
    }

    @Test
    fun `should correctly transform input into a list of reports consisting of integers`() {
        val input = getInput(object {}::class.java.getResource("../test_input")?.toURI())
        val reports = convertLinesToReports(input)
        assertThat(reports).isEqualTo(reportsFixture)
    }

    @Test
    fun `should correctly identify safe lists`() {
        val reports = reportsFixture
        val safeReports = filterUnsafeReports(reports)
        assertThat(safeReports).isEqualTo(safeReportsFixture)
    }

}
