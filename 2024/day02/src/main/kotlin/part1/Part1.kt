package part1

import java.io.File
import java.net.URI
import kotlin.math.absoluteValue


fun main() {
    val input = getInput(object {}::class.java.getResource("../input")?.toURI())
    println(
        filterUnsafeReports(
            convertLinesToReports(input)
        ).size
    )
}

fun getInput(inputURI: URI?): List<String> {
    requireNotNull(inputURI) { "Input URI cannot be null" }
    return File(inputURI).readLines()
}

fun convertLinesToReports(input: List<String>): List<List<Int>> = buildList {
    input.forEach { line ->
        val split = line.split(" ")
        val report = split.map { it.toInt() }
        add(report)
    }
}

fun filterUnsafeReports(reports: List<List<Int>>): List<List<Int>> = reports.filter { report ->
    val reportAsAscending =
        if (report.first() > report.last()) report.reversed()
        else report

    reportAsAscending.forEachIndexed { i, it ->
        if (i != 0 && (
                    reportAsAscending[i - 1] >= it || (it - reportAsAscending[i - 1]).absoluteValue > 3)
        ) return@filter false
    }
    true
}
