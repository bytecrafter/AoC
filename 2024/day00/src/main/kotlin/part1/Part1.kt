package part1

import java.io.File
import java.net.URI


fun main() {
    val input = getInput(object {}::class.java.getResource("../input")?.toURI())
}

fun getInput(inputURI: URI?): List<String> {
    requireNotNull(inputURI) { "Input URI cannot be null" }
    return File(inputURI).readLines()
}
